import React from 'react';
import Layout from '../layouts/index';
import Hero from '../components/hero/Hero';
// import HeroIllustration from '../components/hero/HeroIllustration';
import HeroIllustration from '../components/hero/HeroIllustrationDynamic';
import data from '../data'

const IndexPage = () => {
  return (
    <Layout>
      <Hero
        title= { data.header }
        content= { data.description } 
        illustration={HeroIllustration}
        data={data}
      />
    </Layout>
  )
}

export default IndexPage;
